import React from "react";

import './inputs.css';

function Inputs() {
    const inputValue = document.getElementById('input1')
    const minValue = -10000000;
    const maxValue = 10000000;
    const Value = 100000
    const formatter = new Intl.NumberFormat("ru")
    formatter.format(Value)
    inputValue.onblur = function (e) {
        if (e.value > maxValue) {
            inputValue.value = maxValue
        }
        if (e.value < minValue) {
            inputValue.value = minValue
        }
    }
    
    return (
        <div className="price">
            <input id="input1" min={minValue}  max={maxValue} type="number" placeholder=" "   >

            </input>
            <label> Стоимость</label>
        </div>

    )

}
export default Inputs;